FROM debian:latest

RUN apt-get update && apt-get install -y \
        man \
        less \
        mdbtools \
        postgresql-client && \
    echo "In order to work interactively, mount a volume to /opt/mdbdata before starting this docker container." >> "/opt/README" && \
    echo "Example: docker run -it --rm -v /path/to/host/directory:/opt/mdbdata rillke/mdbtools-docker bash" >> "/opt/README"

COPY scripts/* /usr/bin

# set pager used by `man` to less
ENV PAGER="less"

WORKDIR "/opt/mdbdata"
